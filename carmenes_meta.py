import influxdb
from IPython import embed


class CarmenesMETA:
    def __init__(self, host='lx50'):
        self.vis_client = influxdb.InfluxDBClient(host=host, database='VIS')
        self.nir_client = influxdb.InfluxDBClient(host=host, database='NIR')
        self.vis_sci_client = influxdb.InfluxDBClient(host=host, database='VIS_SCI')
        self._sensor_list_vis = [v['name'] for v in self.vis_client.get_list_measurements()]
        self._sensor_list_nir = [v['name'] for v in self.nir_client.get_list_measurements()]
        self._header_list_vis = [v['name'] for v in self.vis_sci_client.get_list_measurements()]
        self._header_list_nir = [] #TODO: upload NIR data
        self.df_client = influxdb.DataFrameClient(host='lx50')

    def list_sensors(self, channel='both'):
        """
        Returns list of all available sensors in database

        Args:
            channel (str): Can be 'both', 'vis' or 'nir'

        Returns:
            (list): Available sensors

        """
        if channel.casefold() == 'both'.casefold():
            return self._sensor_list_vis + self._sensor_list_nir
        elif channel.casefold() == 'nir'.casefold():
            return self._sensor_list_nir
        elif channel.casefold() == 'vis'.casefold():
            return self._sensor_list_vis
        else:
            raise ValueError('Channel must be "both", "vis" or "nir".')

    def list_header(self, channel='both'):
        if channel.casefold() == 'both'.casefold():
            return self._header_list_vis + self._header_list_nir
        elif channel.casefold() == 'nir'.casefold():
            raise NotImplementedError("NIR channel not implemented yet")
        elif channel.casefold() == 'vis'.casefold():
            return self._header_list_vis
        else:
            raise ValueError('Channel must be "both", "vis" or "nir".')

    def get_metadata_raw(self, sensor, start_date, end_date):
        """
        Get raw sensor data in given time interval.

        Args:
            sensor (str): name of sensor
            start_date (str): start datetime as ISO8601 string
            end_date (str): end datetime as ISO8601 string

        Returns:
            list(Union[bool, float]): raw sensor data
        """
        if sensor in self._sensor_list_vis:
            db = 'VIS'
        elif sensor in self._sensor_list_nir:
            db = 'NIR'
        else:
            raise ValueError(f'Sensor {sensor} not found in database.')

        result = self.df_client.query(
                f'SELECT * FROM "{sensor}" WHERE time >= \'{start_date}\' AND time <= \'{end_date}\'', database=db)
        return result[sensor]

    def get_metadata_median(self, sensor, start_date, end_date):
        """
        Returns median sensor value in given time interval.
        Aggregation happens on server.

        Args:
            sensor (str): name of sensor
            start_date (str): start datetime as ISO8601 string
            end_date (str): end datetime as ISO8601 string

        Returns:
            (float): median sensor value
        """
        return self.get_metadata_special(sensor, 'MEDIAN', start_date, end_date)

    def get_metadata_mean(self, sensor, start_date, end_date):
        """
        Returns mean sensor value in given time interval.
        Aggregation happens on server.

        Args:
            sensor (str): name of sensor
            start_date (str): start datetime as ISO8601 string
            end_date (str): end datetime as ISO8601 string

        Returns:
            (float): mean sensor value
        """
        return self.get_metadata_special(sensor, 'MEAN', start_date, end_date)

    def get_metadata_special(self, sensor, function, start_date, end_date):
        """
        Returns aggregated/selected/transformed sensor value in given time interval.
        Calculation happens on server.

        InfluxDB will aggregate / select / transform the data with the given 'function'.
        For a full list of available functions see.
        https://docs.influxdata.com/influxdb/v1.7/query_language/functions/
        Most functions will work such as: MAX, MIN, SPREAD, STDDEV, MEAN, MEDIAN etc.

        Args:
            sensor (str): name of sensor
            function (str): function name of aggregation/selection to be performed
            start_date (str): start datetime as ISO8601 string
            end_date (str): end datetime as ISO8601 string

        Returns:
            (float): sensor value
        """
        if sensor in self._sensor_list_vis:
            db = 'VIS'
        elif sensor in self._sensor_list_nir:
            db = 'NIR'
        else:
            raise ValueError(f'Sensor {sensor} not found in database.')

        result = self.df_client.query(
                f'SELECT {function}(*) FROM "{sensor}" WHERE time >= \'{start_date}\' AND time <= \'{end_date}\'', database=db)

        return result[sensor].values[0][0]

    def get_header_special(self, keyword, function, start_date, end_date):
        """
        Returns aggregated/selected/transformed sensor value in given time interval.
        Calculation happens on server.

        InfluxDB will aggregate / select / transform the data with the given 'function'.
        For a full list of available functions see.
        https://docs.influxdata.com/influxdb/v1.7/query_language/functions/
        Most functions will work such as: MAX, MIN, SPREAD, STDDEV, MEAN, MEDIAN etc.

        Args:
            sensor (str): name of sensor
            function (str): function name of aggregation/selection to be performed
            start_date (str): start datetime as ISO8601 string
            end_date (str): end datetime as ISO8601 string

        Returns:
            (float): sensor value
        """
        if keyword in self._header_list_vis:
            db = 'VIS_SCI'
        # elif sensor in self._sensor_list_nir:
        #     db = 'NIR'
        else:
            raise ValueError(f'Header keyword {keyword} not found in database.')

        result = self.df_client.query(
                f'SELECT {function}(*) FROM "{keyword}" WHERE time >= \'{start_date}\' AND time <= \'{end_date}\'', database=db)

        return result[keyword].values[0][0]

    def get_header_raw(self, keyword, start_date, end_date):
        """
        Get raw sensor data in given time interval.

        Args:
            sensor (str): name of sensor
            start_date (str): start datetime as ISO8601 string
            end_date (str): end datetime as ISO8601 string

        Returns:
            list(Union[bool, float]): raw sensor data
        """
        if keyword in self._header_list_vis:
            db = 'VIS_SCI'
        # elif sensor in self._sensor_list_nir:
        #     db = 'NIR'
        else:
            raise ValueError(f'Header keyword {keyword} not found in database.')

        result = self.df_client.query(
                f'SELECT * FROM "{keyword}" WHERE time >= \'{start_date}\' AND time <= \'{end_date}\'', database=db)
        try:
            return result[keyword]
        except KeyError:
            return None

if __name__ == '__main__':
    c = CarmenesMETA()
    embed()

