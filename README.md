CarmenesMETA
============

Tool to query CARMENES meta data / sensor data.
Only works within LSW network.

Installation
------------
Required python packages are *influxdb* (and IPython for interactive usage).

Install via pip:

    pip install -r requirements.txt

Usage
-----
From *carmenes_meta.py*  import **CarmenesMETA** class into your project or use it interactively.

    python get_from_influx.py

then

    c.list_sensors()
    ['CAL-Room-Temp',
    'FP_TermoPump_InternalTemp_K',
    'FP_TermoPump_SetPoint_K',
    ...
    
    c.get_metadata_median('CAL-Room-Temp', '2017-08-18T00:00:00Z', '2017-08-18T00:18:00Z') 
    286.29

Check function documentation for further details. 

Crawler
-------
The crawler.py reads the raw data from CAHA. Add to crontab for continuous crawling...
