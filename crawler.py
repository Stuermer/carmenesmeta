#!/home/jstuermer/anaconda3/bin/python

from io import StringIO
import pandas as pd
import requests
import lxml.html
from datetime import datetime
import logging
from influxdb import DataFrameClient

logger = logging.getLogger('Importer')
logger.setLevel(logging.DEBUG)
logging.basicConfig(format='%(levelname)s : %(message)s', level=logging.INFO)

if __name__ == '__main__':
    month = datetime.today().strftime('%Y%m')


    channels = ['VIS', 'NIR']
    for c in channels:
        client = DataFrameClient('localhost', database=c)
        base = f'https://carmenes.caha.es/interlocks/{c}/{month}/'
        dom = lxml.html.fromstring(requests.get(base).content)

        for link in dom.xpath('//a/@href'):  # select the url in href for all a tags(links)
            if '.dat' in link:
                logger.info(f'Read from {base+link}')
                sensor_name = link[:-4]
                d = requests.get(base+link).content
                data = StringIO(str(d, 'utf-8'))
                try:
                    df = pd.read_csv(data, names=['Date', 'value'], index_col=0, parse_dates=True, skipfooter=1,
                                     engine='python')
                    logger.debug(df)
                    logger.debug(df.dtypes)
                    df.dropna(inplace=True)
                    name_split = sensor_name.split('-')
                    if df.dtypes.values[0] in [float, bool]:
                        client.write_points(df, sensor_name, batch_size=10000)
                        logger.info(f'Successfully wrote data from {base+link}')
                    else:
                        logger.warning(f'Could not read and convert data in {base+link}')
                except pd.errors.EmptyDataError:
                    logger.warning(f'No data in {base+link}')
