from astropy.io import fits as fits
import influxdb
import glob
# InfluxDB connections settings
host = 'localhost'
port = 8086
user = 'root'
password = 'root'
dbname = 'VIS_SCI'

myclient = influxdb.InfluxDBClient(host, port, user, password, dbname)
# myclient.drop_database('VIS_SCI')
# myclient.create_database('VIS_SCI')
failed = []
for f in glob.glob('/data/CARM_VIS/v2.20/red/2020-06-*/*.fits', recursive=True):
    try:
        print(f)
        h = fits.getheader(f)

        exclude_list = ['RADESYS', "EQUINOX", "PROG-NUM", "PROG-PI", "TELESCOP", "INSTRUME", "REFERENC", "ORIGIN", "COMMENT1", "COMMENT2"]
        json_body = []
        for i, g in enumerate(h):
            if i>8:
                if not g in exclude_list:
                    if not (("ETALON DURATION" in g) or ("ETALON START" in g)):
                        val = h[g]
                        if isinstance(val, str):
                            if val.strip() == "":
                                continue
                        if isinstance(val, int):
                            val = float(val)
                        if val is None:
                            continue
                        json_body.append(
                            {
                                "measurement": g.replace(" ", "."),
                                "time": h["DATE-OBS"],
                                "fields": {"value": val}
                            }
                        )

        print(json_body)
        myclient.write_points(json_body, batch_size=100)
    except:

        failed.append(f)

print(failed)
